INSTALLING MODULE

1) INSTALATION FROM SOURCE
  a) Download and change to directory containing

    PACKAGE.tar.gz or PACKAGE.zip
    
  b) Extract the source by (tar xfv) or unzip it.

  c) Change to the created folder and run:

        python bdist install

2) INSTALLING FROM BINARIES SPECIAL TO YOUR PLATFORM
    a) WINDOWS
        Just download and run PACKAGE.msi or PACKAGE.exe
    b) RPM files for REDHAT like distribution
        Install a package
        rpm –ivh PACKAGE.rpm
        upgrade a package
        rpm –Uvh PACKAGE.rpm 

