import random

def add(x,y):
    return x+y

def multiply(x,y):
    return  x*y

class Test(object):
    def __init__(self, Name = "Ondra", reward = 1):
        self.name = Name
        self.test_taken = 0
        self.average = 0.0
        self.taken = 0
        self.reward = reward 


    def runTest(self):
        print "Ahoj %s\n" % self.name
        x = random.randint(1,10)
        y = random.randint(5,15)
        try:
            raw = raw_input("How much is %d * %d?\n" % (x,y))
            standard = multiply(x,y)
            user = int(raw)
            if( user == standard ): 
                print("Well done!\n")
                self.average = ( self.average * ( float(self.taken) / (self.taken + 1) ) ) + ( float(self.reward) / (self.taken+1) )
            else:
                print("Bad choice!\n")
            self.taken = self.taken + 1
        except : 
            print("You provided no number. Try it next time")

    def PrintResults(self):
        print("%s score average %f out of %f in %d attempts" % (self.name, self.average, self.reward, self.taken))
       
if __name__ == "__main__" :
    ondraTest = Test()
    while ondraTest.taken < 5 :
       ondraTest.runTest()

    ondraTest.PrintResults()
