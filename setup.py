from distutils.core import setup, Extension


setup(      name = 'myMath'
        ,   version = '0.01'
        ,   description = 'Python pure Library with one class and few functions'
        ,   author = 'Ondrej Platek'
        ,   author_email = 'guesswhat@notvalid.com'
        ,   url = 'http://oplatek.wordpress.com/'
        ,   download_url = 'todo could be handy'
        # implies myMath is in the root dir and exist myCheck dir with myCheck/__init__py and myCheck/check.py as well
        ,   py_modules = ['myMath','myCheck.check'] 
        # promising that the Distutils will find a file highMath/__init__.py  
        ,   packages = ['highMath'] 
        # math.c is located in cMath/src directory and the Python module would be named cMath
#        ,   ext_Modules[Extension('cMath.cMath'
#                    , ['src/math.c'] 
#                    , include_dirs=['myNonSystemHeaders']
#                    )] 
        # the SWIG files handling find at: http://docs.python.org/distutils/setupscript.html#describing-extension-modules
#        ,  ext_modules=[Extension('_foo', ['foo.i'],
#                                 swig_opts=['-modern', '-I../include'])]
#        ,  py_modules=['foo']

        ,  # is the last empty parameter
     )
    
