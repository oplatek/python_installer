MAKING ARCHIVE:
for windows only on windows only!
    python setup.py bdist_msi
    python setup.py bdist_wininst

for linux only on linux
    python setup.py bdist_rpm
   
python setup.py bdist
    creates on linux tar.gz
    creates on Windows zip
python setup.py bdist --formats=gztar,zip
    creates both but zip and tar.gz independetly of the platform


IMPORTANT ISSUE: compilation with extension for each PYTHON separately on Windows and only from Windows!
If you have a non-pure distribution, the extensions can only be created on a Windows platform, and will be Python version dependent. The installer filename will reflect this and now has the form foo-1.0.win32-py2.0.exe. You have to create a separate installer for every Python version you want to support.
